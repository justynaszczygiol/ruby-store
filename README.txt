*** DESCRIPTION ***
ruby is an on-line store application allowing customers to purchase personalized jewelry.
It is dedicated to the customers interested in unique, personalized jewelry as well as managers of the store.
This application allows customers to choose gemstones, precious metal, style of his/her dreamed jewelry and order it by choosing convenient type of shipment and method of payment.
It also helps administrators to manage the store by adjusting offers and setting discounts.

