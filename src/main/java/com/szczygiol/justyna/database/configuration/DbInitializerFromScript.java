package com.szczygiol.justyna.database.configuration;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collectors;

public class DbInitializerFromScript {
    DbConnectionManager dcm = new DbConnectionManager();

    private String getStatement() {
        String scripts = "";
        try (BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/ruby_scripts.sql"))) {
            scripts = reader.lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return scripts;
    }

    public void initialize() {
        try {
            Connection connection = dcm.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(getStatement());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
