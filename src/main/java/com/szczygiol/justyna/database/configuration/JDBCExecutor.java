package com.szczygiol.justyna.database.configuration;


import com.szczygiol.justyna.database.content.dao.*;
import com.szczygiol.justyna.database.content.objects.*;
import com.szczygiol.justyna.database.content.util.DAOException;

import java.sql.Connection;
import java.sql.SQLException;


public class JDBCExecutor {
    public static void main(String[] args) {
        DbConnectionManager dcm = new DbConnectionManager();

//        DbInitializerFromScript dbInitializer = new DbInitializerFromScript();
//        dbInitializer.initialize();

        try {
            Connection connection = dcm.getConnection();

            CustomerDAO customerDAO = new CustomerDAO(connection);
            DeliveryDetailsDAO delivery_detailsDAO= new DeliveryDetailsDAO(connection);
            GemDAO gemDAO = new GemDAO(connection);
            JewelryDAO jewelryDAO = new JewelryDAO(connection);
            StyleDAO styleDAO = new StyleDAO(connection);
            PreciousMetalDAO precious_metalDAO = new PreciousMetalDAO(connection);
            PaymentDAO paymentDAO = new PaymentDAO(connection);
            ShipmentDAO shipmentDAO = new ShipmentDAO(connection);
            OrdersDAO orderDAO = new OrdersDAO(connection);


//            -- TESTING CUSTOMER --
//            - insert -
            Customer customer = new Customer();
//            customer.setFirst_name("Jesse");
//            customer.setLast_name("Pinkman");
//            customer.setNickname("Heisenberg");
//            customer.setPassword("meta");
//            customer.setEmail("walter.white@chemistry.com");
//            customer.setPhone_number("854-721-228");
//            customer.setSubscription(true);
//            customer.setSum_of_orders(0);
//            customer.setPersonal_discount(0);
//            customerDAO.create(customer);
//            - find all -
//            System.out.println(customerDAO.findAll());
////            - find by id -
//            customer = customerDAO.findById(1);
//            System.out.println(customer);
////            - update -
//            customer = customerDAO.findById(1);
//            customer.setEmail("new.email@walter.white.com");
//            customer = customerDAO.update(customer);
//            System.out.println(customer);
////            - delete -
//            Customer dbCustomer = customerDAO.create(customer);
//            System.out.println(dbCustomer);
//            dbCustomer = customerDAO.findById(dbCustomer.getId());
//            System.out.println(dbCustomer);
//            dbCustomer.setEmail("jeeeee.com");
//            dbCustomer = customerDAO.update(dbCustomer);
//            System.out.println(dbCustomer);
//            customerDAO.delete(dbCustomer.getId());
//            System.out.println(customerDAO.findAll().toString());

//            -- TESTING DELIVERY-DETAILS --
////            - insert -
//            Delivery_details dd = new Delivery_details();
//            dd.setFirst_name("Walter");
//            dd.setLast_name("White");
//            dd.setStreet("Oaky Street");
//            dd.setHouse_number("8A");
//            dd.setZip_code("44-784");
//            dd.setCity("Albuquerque");
//            dd.setCountry("USA");
//            dd.setPhone_number("785-452-123");
//            dd.setEmail("walty@walty.com");
//            delivery_detailsDAO.create(dd);
//////            - find all -
//            System.out.println(delivery_detailsDAO.findAll());
//////            - find by id -
//            System.out.println(delivery_detailsDAO.findById(2));
//////            - update -
//            dd.setEmail("walter@metaproducer.com");
//            delivery_detailsDAO.update(dd);
//            System.out.println(dd);
////            - delete -
//            System.out.println(delivery_detailsDAO.findAll());
//            delivery_detailsDAO.delete(7);
//            System.out.println(delivery_detailsDAO.findAll());

//            -- TESTING GEM --
////            - insert -
//            Gem gem = new Gem();
//            gem.setDescription("ruby");
//            gem.setColour("red");
//            gem.setPrice_variable(0.11);
//            gemDAO.create(gem);
////            - find all -
//            System.out.println(gemDAO.findAll());
////            - find by id -
//            gem = gemDAO.findById(1);
//            System.out.println(gem);
////            - update -
//            gem.setColour("green");
//            gemDAO.update(gem);
//            System.out.println(gem);
////            - delete -
//            Gem gemCopy = gemDAO.create(gem);
//            gemCopy.setDescription("diamond");
//            gemCopy = gemDAO.update(gemCopy);
//            System.out.println(gemDAO.findAll());
//            gemDAO.delete(gemCopy.getId());
//            System.out.println(gemDAO.findAll());

//            -- TESTING JEWELRY --
//            - insert -
//            Jewelry jewelry = new Jewelry();
//            jewelry.setStyle_id(styleDAO.findById(12));
//            jewelry.setMain_gem(gemDAO.findById(2));
//            jewelry.setSecondary_gem(gemDAO.findById(1));
//            jewelry.setPrecious_metal(precious_metalDAO.findById(1));
//            jewelry.setEngraver_text("lovely");
//            jewelryDAO.create(jewelry);
////            - find all -
//            System.out.println(jewelryDAO.findAll());
//            - find by id -
//            System.out.println(jewelryDAO.findById(8));
//            - update -
//            jewelry.setDescription("not so good");
//            jewelryDAO.update(jewelryDAO.findById(13));
//            System.out.println(jewelryDAO.findAll());
//            jewelryDAO.delete(13);
//            System.out.println(jewelryDAO.findAll());


//            -- TESTING STYLE --
////            - insert -
//            Style style = new Style();
//            style.setType("Ring");
//            style.setDescription("Beautiful ring");
//            style.setCollection("Beautiful");
//            style.setMain_gem_quantity(0);
//            style.setSecondary_gem_quantity(10);
//            style.setMain_gem_cut("Round");
//            style.setMain_gem_size(0.11);
//            style.setSecondary_gem_cut("Round");
//            style.setSecondary_gem_size(0.01);
//            style.setEngraver(true);
//            style.setPrecious_metal_variable(0.11);
//            styleDAO.create(style);
////            - find all -
//            System.out.println(styleDAO.findAll());
////            - find by id -
//            style = styleDAO.findById(1);
//            System.out.println(style);
////            - update -
//            style.setType("Earring");
//            styleDAO.update(style);
//            System.out.println(style);
////            - delete -
//            Style styleCopy = style;
//            styleDAO.create(styleCopy);
//            styleCopy.setDescription("diamond");
//            styleCopy = styleDAO.update(styleCopy);
//            System.out.println(styleDAO.findAll());
//            gemDAO.delete(styleCopy.getId());
//            System.out.println(styleDAO.findAll());

//              -- TESTING PAYMENT --
////            - insert -
//            Payment payment = new Payment();
//            payment.setMethod("Przelew");
//            paymentDAO.create(payment);
////            - find all -
//            System.out.println(paymentDAO.findAll());
////            - find by id -
//            payment = paymentDAO.findById(1);
//            System.out.println(payment);
////            - update -
//            payment.setStatus("Complete");
//            paymentDAO.update(payment);
//            System.out.println(payment);
//            System.out.println(paymentDAO.findById(payment.getId()));
////            - delete -
//            System.out.println(paymentDAO.findAll());
//            Payment paymentCopy = paymentDAO.create(payment);
//            paymentCopy.setStatus("change");
//            paymentCopy = paymentDAO.update(paymentCopy);
//            System.out.println(paymentDAO.findAll());
//            paymentDAO.delete(paymentCopy.getId());
//            System.out.println(paymentDAO.findAll());

//            -- TESTING SHIPMENT --
////            - insert -
//            Shipment shipment = new Shipment();
//            shipment.setMethod("DHL");
//            shipment.setPrice(8.99);
//            shipmentDAO.create(shipment);
////            - find all -
//            System.out.println(shipmentDAO.findAll());
////            - find by id -
//            System.out.println(shipmentDAO.findById(1));
////            - update -
//            shipment = shipmentDAO.findById(1);
//            shipment.setMethod("poczta polska");
//            shipmentDAO.update(shipment);
//            System.out.println(shipmentDAO.findAll());
////            - delete -
//            shipmentDAO.delete(shipment.getId());
//            System.out.println(shipmentDAO.findAll());

//            -- TESTING PRECIOUS_METAL --
////            - insert -
//            Precious_metal pm = new Precious_metal();
//            pm.setType("gold");
//            pm.setDescription("rose gold");
//            pm.setCarats(16);
//            pm.setPurity(985);
//            pm.setPrice_variable(0.185);
//            precious_metalDAO.create(pm);
////            - find all -
//            System.out.println(precious_metalDAO.findAll());
////            - find by id -
//            System.out.println(precious_metalDAO.findById(1));
////            - update -
//            pm = precious_metalDAO.findById(1);
//            pm.setDescription("tinky-whinky");
//            precious_metalDAO.update(pm);
//            System.out.println(precious_metalDAO.findAll());
////            - delete -
//            precious_metalDAO.delete(5);
//            System.out.println(precious_metalDAO.findAll());


//            -- TESTING ORDERS --
////            - insert -
            Orders order = new Orders();
//            order.setCustomer_id(customerDAO.findById(1));
//            order.setJewelry_price(1587.99);
//            order.setJewelry_discounted_price(1587.99);
//            order.setTotal_price(1587.99);
//            order.setDelivery_details_id(delivery_detailsDAO.findById(1));
//            order.setShipment_id(shipmentDAO.findById(2));
//            order.setPayment_id(paymentDAO.findById(1));
//            order.setStatus("tesing discount");
//            orderDAO.create(order);
//            - find all -
//            System.out.println(orderDAO.findAll());
////            - find by id -
//            System.out.println(orderDAO.findById(4));
////            - update -
//            order = orderDAO.findById(2);
//            order.setStatus("tinky-whinky");
//            orderDAO.update(order);
//            System.out.println(orderDAO.findAll());
////            - delete -
//            orderDAO.delete(2);
//            System.out.println(orderDAO.findAll());
//            - get sum of orders of customer -
//            System.out.println(customerDAO.findById(1));

//            -- find by ... in style --
//            System.out.println(styleDAO.findByType("Ring"));
//            System.out.println(styleDAO.findByCollection("Illusion"));

//            -- TEST ORDERS_JEWELRY --
            OrdersJewelryDAO orders_jewelryDAO = new OrdersJewelryDAO(connection);
//            - insert -
            OrdersJewelry oj = new OrdersJewelry(orderDAO.findById(5),jewelryDAO.findById(1));
            orders_jewelryDAO.create(oj);
//            System.out.println(orders_jewelryDAO.findById(1));
            System.out.println(orders_jewelryDAO.findAll());
//            System.out.println(orders_jewelryDAO.findByOrder(5));
//            System.out.println(orders_jewelryDAO.findByJewelry(4));
//            oj.setJewelry_id(jewelryDAO.findById(4));
//            orders_jewelryDAO.update(oj);
            System.out.println(orders_jewelryDAO.findAll());
            orders_jewelryDAO.delete(4);
            System.out.println(orders_jewelryDAO.findAll());







        } catch (SQLException | DAOException e) {
            e.printStackTrace();
        }
    }
}
