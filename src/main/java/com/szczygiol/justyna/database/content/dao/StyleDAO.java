package com.szczygiol.justyna.database.content.dao;


import com.szczygiol.justyna.database.content.objects.Style;
import com.szczygiol.justyna.database.content.util.DAOException;
import com.szczygiol.justyna.database.content.util.DataAccessObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StyleDAO extends DataAccessObject<Style> {
    private static final String INSERT = "INSERT INTO style (type, collection, description, main_gem_quantity," +
            "secondary_gem_quantity, main_gem_cut, main_gem_size, secondary_gem_cut, secondary_gem_size, " +
            "engraver, precious_metal_variable, main_gem_variable, secondary_gem_variable) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String GET_ONE = "SELECT id, type, collection, description, " +
            "main_gem_quantity, secondary_gem_quantity, main_gem_cut, main_gem_size, " +
            "secondary_gem_cut, secondary_gem_size, engraver, precious_metal_variable, " +
            "main_gem_variable, secondary_gem_variable FROM style WHERE id = ?";
    private static final String UPDATE = "UPDATE style SET type = ?, collection = ?, description = ?," +
            "main_gem_quantity = ?,  secondary_gem_quantity = ?, main_gem_cut = ?, main_gem_size = ?, " +
            "secondary_gem_cut = ?, secondary_gem_size = ?, engraver = ?, precious_metal_variable = ?, " +
            "main_gem_variable = ?, secondary_gem_variable = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM style WHERE id = ?";
    private static final String FIND_ALL = "SELECT id, type, collection, description, " +
            "main_gem_quantity, secondary_gem_quantity, main_gem_cut, main_gem_size, " +
            "secondary_gem_cut, secondary_gem_size, engraver, precious_metal_variable, " +
            "main_gem_variable, secondary_gem_variable FROM style";
    private static final String FIND_BY_TYPE = "SELECT id, type, collection, description, " +
            "main_gem_quantity, secondary_gem_quantity, main_gem_cut, main_gem_size, " +
            "secondary_gem_cut, secondary_gem_size, engraver, precious_metal_variable, " +
            "main_gem_variable, secondary_gem_variable FROM style WHERE type = ?";
    private static final String FIND_BY_COLLECTION = "SELECT id, type, collection, description, " +
            "main_gem_quantity, secondary_gem_quantity, main_gem_cut, main_gem_size, " +
            "secondary_gem_cut, secondary_gem_size, engraver, precious_metal_variable, " +
            "main_gem_variable, secondary_gem_variable FROM style WHERE collection = ?";


    public StyleDAO(Connection connection) {
        super(connection);
    }


    @Override
    public Style findById(long id) throws DAOException, SQLException {
        Style style = new Style();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE);) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    style.setId(rs.getLong("id"));
                    style.setType(rs.getString("type"));
                    style.setCollection(rs.getString("collection"));
                    style.setDescription(rs.getString("description"));
                    style.setMainGemQuantity(rs.getInt("main_gem_quantity"));
                    style.setSecondaryGemQuantity(rs.getInt("secondary_gem_quantity"));
                    style.setMainGemCut(rs.getString("main_gem_cut"));
                    style.setMainGemSize(rs.getDouble("main_gem_size"));
                    style.setSecondaryGemCut(rs.getString("secondary_gem_cut"));
                    style.setSecondaryGemSize(rs.getDouble("secondary_gem_size"));
                    style.setEngraver(rs.getBoolean("engraver"));
                    style.setPreciousMetalVariable(rs.getDouble("precious_metal_variable"));
                    style.setMain_gem_variable();
                    style.setSecondary_gem_variable();
                }
            }
        } catch (SQLException e) {
            throw new DAOException("No object of id " + id + " have been found in the style", e);
        } finally {
            connection.close();
        }
        return style;
    }

    @Override
    public List<Style> findAll() throws DAOException, SQLException {
        List<Style> styles = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Style style = new Style();
                    style.setId(rs.getLong("id"));
                    style.setType(rs.getString("type"));
                    style.setCollection(rs.getString("collection"));
                    style.setDescription(rs.getString("description"));
                    style.setMainGemQuantity(rs.getInt("main_gem_quantity"));
                    style.setSecondaryGemQuantity(rs.getInt("secondary_gem_quantity"));
                    style.setMainGemCut(rs.getString("main_gem_cut"));
                    style.setMainGemSize(rs.getDouble("main_gem_size"));
                    style.setSecondaryGemCut(rs.getString("secondary_gem_cut"));
                    style.setSecondaryGemSize(rs.getDouble("secondary_gem_size"));
                    style.setEngraver(rs.getBoolean("engraver"));
                    style.setPreciousMetalVariable(rs.getDouble("precious_metal_variable"));
                    style.setMain_gem_variable();
                    style.setSecondary_gem_variable();
                    styles.add(style);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Read data from style failed", e);
        } finally {
            connection.close();
        }
        return styles;
    }

    public List<Style> findByType(String type) throws DAOException, SQLException {
        List<Style> stylesOfType = new ArrayList<>();
        try (PreparedStatement statement = this.connection.prepareStatement(FIND_BY_TYPE)) {
            statement.setString(1, type);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Style style = new Style();
                    style.setId(rs.getLong("id"));
                    style.setType(rs.getString("type"));
                    style.setCollection(rs.getString("collection"));
                    style.setDescription(rs.getString("description"));
                    style.setMainGemQuantity(rs.getInt("main_gem_quantity"));
                    style.setSecondaryGemQuantity(rs.getInt("secondary_gem_quantity"));
                    style.setMainGemCut(rs.getString("main_gem_cut"));
                    style.setMainGemSize(rs.getDouble("main_gem_size"));
                    style.setSecondaryGemCut(rs.getString("secondary_gem_cut"));
                    style.setSecondaryGemSize(rs.getDouble("secondary_gem_size"));
                    style.setEngraver(rs.getBoolean("engraver"));
                    style.setPreciousMetalVariable(rs.getDouble("precious_metal_variable"));
                    style.setMain_gem_variable();
                    style.setSecondary_gem_variable();
                    stylesOfType.add(style);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Finding type failed", e);
        } finally {
            connection.close();
        }
        return stylesOfType;
    }

    public List<Style> findByCollection (String collection) throws DAOException, SQLException {
        List<Style> stylesOfType = new ArrayList<>();
        try (PreparedStatement statement = this.connection.prepareStatement(FIND_BY_COLLECTION)) {
            statement.setString(1, collection);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Style style = new Style();
                    style.setId(rs.getLong("id"));
                    style.setType(rs.getString("type"));
                    style.setCollection(rs.getString("collection"));
                    style.setDescription(rs.getString("description"));
                    style.setMainGemQuantity(rs.getInt("main_gem_quantity"));
                    style.setSecondaryGemQuantity(rs.getInt("secondary_gem_quantity"));
                    style.setMainGemCut(rs.getString("main_gem_cut"));
                    style.setMainGemSize(rs.getDouble("main_gem_size"));
                    style.setSecondaryGemCut(rs.getString("secondary_gem_cut"));
                    style.setSecondaryGemSize(rs.getDouble("secondary_gem_size"));
                    style.setEngraver(rs.getBoolean("engraver"));
                    style.setPreciousMetalVariable(rs.getDouble("precious_metal_variable"));
                    style.setMain_gem_variable();
                    style.setSecondary_gem_variable();
                    stylesOfType.add(style);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Finding type failed", e);
        } finally {
            connection.close();
        }
        return stylesOfType;
    }

    @Override
    public Style update(Style dto) throws DAOException, SQLException {
        Style style;
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE);) {
            statement.setString(1, dto.getType());
            statement.setString(2, dto.getCollection());
            statement.setString(3, dto.getDescription());
            statement.setInt(4, dto.getMainGemQuantity());
            statement.setInt(5, dto.getSecondaryGemQuantity());
            statement.setString(6, dto.getMainGemCut());
            statement.setDouble(7, dto.getMainGemSize());
            statement.setString(8, dto.getMainGemCut());
            statement.setDouble(9, dto.getMainGemSize());
            statement.setBoolean(10, dto.isEngraver());
            statement.setDouble(11, dto.getPreciousMetalVariable());
            statement.setDouble(12, dto.getMainGemVariable());
            statement.setDouble(13, dto.getSecondaryGemVariable());
            statement.setLong(14, dto.getId());
            statement.execute();
            style = this.findById(dto.getId());
        } catch (SQLException e) {
            throw new DAOException("Updating style failed", e);
        } finally {
            connection.close();
        }
        return style;
    }

    @Override
    public Style create(Style dto) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, dto.getType());
            statement.setString(2, dto.getCollection());
            statement.setString(3, dto.getDescription());
            statement.setInt(4, dto.getMainGemQuantity());
            statement.setInt(5, dto.getSecondaryGemQuantity());
            statement.setString(6, dto.getMainGemCut());
            statement.setDouble(7, dto.getMainGemSize());
            statement.setString(8, dto.getSecondaryGemCut());
            statement.setDouble(9, dto.getSecondaryGemSize());
            statement.setBoolean(10, dto.isEngraver());
            statement.setDouble(11, dto.getPreciousMetalVariable());
            statement.setDouble(12, dto.getMainGemVariable());
            statement.setDouble(13, dto.getSecondaryGemVariable());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    dto.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Creating style failed", e);
        } finally {
            connection.close();
        }
        return dto;
    }


    @Override
    public void delete(long id) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE);) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Deleting jewelry failed", e);
        } finally {
            connection.close();
        }
    }
}
