package com.szczygiol.justyna.database.content.objects;

import com.szczygiol.justyna.database.content.util.DataTransferObject;

public class DeliveryDetails implements DataTransferObject {
    private long id;
    private String firstName;
    private String lastName;
    private String street;
    private String houseNumber;
    private String zipCode;
    private String city;
    private String country;
    private String phoneNumber;
    private String email;

    public DeliveryDetails() {
    }

    public DeliveryDetails(String firstName, String lastName, String street,
                           String houseNumber, String zipCode, String city,
                           String country, String phoneNumber, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.street = street;
        this.houseNumber = houseNumber;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    @Override
    public String toString() {
        return
                "id=" + id  + " |" +
                " first_name: " + firstName + " |" +
                " last_name: " + lastName + " |" +
                " street: " + street  + " |" +
                " house_number: " + houseNumber + " |" +
                " zip_code: " + zipCode + " |" +
                " city: " + city  + " |" +
                " country: " + country  + " |" +
                " phone_number: " + phoneNumber + " |" +
                " email: " + email + '\n';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
