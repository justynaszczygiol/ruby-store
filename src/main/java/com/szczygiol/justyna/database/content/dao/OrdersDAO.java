package com.szczygiol.justyna.database.content.dao;

import com.szczygiol.justyna.database.content.objects.*;
import com.szczygiol.justyna.database.content.util.DAOException;
import com.szczygiol.justyna.database.content.util.DataAccessObject;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class OrdersDAO extends DataAccessObject<Orders> {

    private static final String INSERT = "INSERT INTO orders (order_date, customer_id, " +
            "jewelry_price, jewelry_discounted_price, total_price, delivery_details_id, " +
            "shipment_id, payment_id, status) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String GET_ONE = "SELECT id, order_date, customer_id, " +
            "jewelry_price, jewelry_discounted_price, total_price, delivery_details_id, " +
            "shipment_id, payment_id, status FROM orders WHERE id = ?";
    private static final String FIND_ALL = "SELECT id, order_date, customer_id, " +
            "jewelry_price, jewelry_discounted_price, total_price, delivery_details_id, " +
            "shipment_id, payment_id, status FROM orders";
    private static final String UPDATE = "UPDATE orders SET order_date = ?, customer_id = ?, " +
            "jewelry_price = ?, jewelry_discounted_price = ?, total_price = ?, delivery_details_id = ?, " +
            "shipment_id = ?, payment_id = ?, status = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM orders WHERE id = ?";

    public OrdersDAO(Connection connection) {
        super(connection);
    }

    @Override
    public Orders findById(long id) throws DAOException, SQLException {
        Orders order = new Orders();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE)) {
            statement.setLong(1, id);
           try (ResultSet rs = statement.executeQuery()) {
               while (rs.next()) {
                   order.setId(rs.getLong("id"));
                   order.setOrderDate(rs.getTimestamp("order_date"));
                   CustomerDAO customerDAO = new CustomerDAO(connection);
                   order.setCustomerId(customerDAO.findById(rs.getLong("customer_id")));
                   order.setJewelryPrice(rs.getDouble("jewelry_price"));
                   order.setJewelryDiscountedPrice(rs.getDouble("jewelry_discounted_price"));
                   order.setTotalPrice(rs.getDouble("total_price"));
                   DeliveryDetailsDAO delivery_detailsDAO = new DeliveryDetailsDAO(connection);
                   order.setDeliveryDetailsId(delivery_detailsDAO.findById(rs.getLong("delivery_details_id")));
                   ShipmentDAO shipmentDAO = new ShipmentDAO(connection);
                   order.setShipmentId(shipmentDAO.findById(rs.getLong("shipment_id")));
                   PaymentDAO paymentDAO = new PaymentDAO(connection);
                   order.setPaymentId(paymentDAO.findById(rs.getLong("payment_id")));
                   order.setStatus(rs.getString("status"));
               }
           }
        } catch (SQLException e) {
            throw new DAOException("No object of id " + id + " have been found in the order", e);
        } finally {
            connection.close();
        }
        return order;
    }

    @Override
    public List<Orders> findAll() throws DAOException, SQLException {
        List<Orders> orders = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Orders order = new Orders();
                    order.setId(rs.getLong("id"));
                    order.setOrderDate(rs.getTimestamp("order_date"));
                    CustomerDAO customerDAO = new CustomerDAO(connection);
                    order.setCustomerId(customerDAO.findById(rs.getLong("customer_id")));
                    order.setJewelryPrice(rs.getDouble("jewelry_price"));
                    order.setJewelryDiscountedPrice(rs.getDouble("jewelry_discounted_price"));
                    order.setTotalPrice(rs.getDouble("total_price"));
                    DeliveryDetailsDAO delivery_detailsDAO = new DeliveryDetailsDAO(connection);
                    order.setDeliveryDetailsId(delivery_detailsDAO.findById(rs.getLong("delivery_details_id")));
                    ShipmentDAO shipmentDAO = new ShipmentDAO(connection);
                    order.setShipmentId(shipmentDAO.findById(rs.getLong("shipment_id")));
                    PaymentDAO paymentDAO = new PaymentDAO(connection);
                    order.setPaymentId(paymentDAO.findById(rs.getLong("payment_id")));
                    order.setStatus(rs.getString("status"));
                    orders.add(order);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Read data from orders failed", e);
        } finally {
            connection.close();
        }
        return orders;
    }

    @Override
    public Orders update(Orders dto) throws DAOException, SQLException {
        Orders order;
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE)) {
            statement.setTimestamp(1, Timestamp.from(Instant.now()));
            statement.setLong(2, dto.getCustomerId().getId());
            statement.setDouble(3, dto.getJewelryPrice());
            statement.setDouble(4, dto.getJewelryDiscountedPrice());
            statement.setDouble(5, dto.getTotalPrice());
            statement.setLong(6, dto.getDeliveryDetailsId().getId());
            statement.setLong(7, dto.getShipmentId().getId());
            statement.setLong(8, dto.getPaymentId().getId());
            statement.setString(9, dto.getStatus());
            statement.setLong(10, dto.getId());
            statement.execute();
            order = this.findById(dto.getId());
        } catch (SQLException e) {
            throw new DAOException("Updating orders failed", e);
        } finally {
            connection.close();
        }
        return order;
    }

    @Override
    public Orders create(Orders dto) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setTimestamp(1, Timestamp.from(Instant.now()));
            statement.setLong(2, dto.getCustomerId().getId());
            statement.setDouble(3, dto.getJewelryPrice());
            statement.setDouble(4, dto.getJewelryDiscountedPrice());
            statement.setDouble(5, dto.getTotalPrice());
            statement.setLong(6, dto.getDeliveryDetailsId().getId());
            statement.setLong(7, dto.getShipmentId().getId());
            statement.setLong(8, dto.getPaymentId().getId());
            statement.setString(9, dto.getStatus());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    dto.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Creating order failed", e);
        } finally {
            connection.close();
        }
            return dto;
    }

    @Override
    public void delete(long id) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Deleting orders failed", e);
        } finally {
            connection.close();
        }
    }
}
