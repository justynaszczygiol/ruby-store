package com.szczygiol.justyna.database.content.objects;

import com.szczygiol.justyna.database.content.util.DataTransferObject;

public class Gem implements DataTransferObject {
    private long id;
    private String description;
    private String colour;
    private double priceVariable;

    @Override
    public String toString() {
        return  "id: " + id + " |" +
                " description: " + description + " |" +
                " colour: " + colour + " |" +
                " price_variable: " + priceVariable + '\n';
    }

    public Gem() {
    }

    public Gem(String description, String colour, double priceVariable) {
        this.description = description;
        this.colour = colour;
        this.priceVariable = priceVariable;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public double getPriceVariable() {
        return priceVariable;
    }

    public void setPriceVariable(double priceVariable) {
        this.priceVariable = priceVariable;
    }
}
