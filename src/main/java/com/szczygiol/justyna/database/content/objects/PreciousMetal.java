package com.szczygiol.justyna.database.content.objects;

import com.szczygiol.justyna.database.content.util.DataTransferObject;


public class PreciousMetal implements DataTransferObject {
    private long id;
    private String type;
    private String description;
    private int carats;
    private int purity;
    private double priceVariable;

    @Override
    public String toString() {
        return  "id: " + id +  " |" +
                " type: " + type +  " |" +
                " description:" + description +  " |" +
                " carats: " + carats +  " |" +
                " purity: " + purity + " |" +
                " price_variable: " + priceVariable + '\n';
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCarats() {
        return carats;
    }

    public void setCarats(int carats) {
        this.carats = carats;
    }

    public int getPurity() {
        return purity;
    }

    public void setPurity(int purity) {
        this.purity = purity;
    }

    public double getPriceVariable() {
        return priceVariable;
    }

    public void setPriceVariable(double priceVariable) {
        this.priceVariable = priceVariable;
    }
}
