package com.szczygiol.justyna.database.content.objects;

import com.szczygiol.justyna.database.content.util.DataTransferObject;

import java.sql.Timestamp;
import java.util.List;

public class Orders implements DataTransferObject {
    private long id;
    private Timestamp orderDate;
    private Customer customerId;
    private double jewelryPrice;
    private double jewelryDiscountedPrice;
    private double totalPrice;
    private DeliveryDetails deliveryDetailsId;
    private Shipment shipmentId;
    private Payment paymentId;
    private String status;
    private List<Jewelry> jewelryList;

    @Override
    public String toString() {
        return  "id: " + id + " |" +
                " order_date: " + orderDate + " |" +
                " customer_id: " + customerId + " |" +
                " jewelry_price: " + jewelryPrice + " |" +
                " jewelry_discounted_price: " + jewelryDiscountedPrice + " |" +
                " total_price: " + totalPrice + " |" +
                " delivery_details_id: " + deliveryDetailsId + " |" +
                " shipment_id: " + shipmentId + " |" +
                " payment_id: " + paymentId + " |" +
                " status: " + status + '\n';
    }

    public Orders() {
    }

    public List<Jewelry> getJewelryList() {
        return jewelryList;
    }

    public void setJewelryList(List<Jewelry> jewelryList) {
        this.jewelryList = jewelryList;
    }

    public double getJewelryPrice() {
        return jewelryPrice;
    }

    public void setJewelryPrice(double jewelryPrice) {
        this.jewelryPrice = jewelryPrice;
    }

    public double getJewelryDiscountedPrice() {
        return jewelryDiscountedPrice;
    }

    public void setJewelryDiscountedPrice(double jewelryDiscountedPrice) {
        this.jewelryDiscountedPrice = jewelryDiscountedPrice;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Timestamp orderDate) {
        this.orderDate = orderDate;
    }

    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public DeliveryDetails getDeliveryDetailsId() {
        return deliveryDetailsId;
    }

    public void setDeliveryDetailsId(DeliveryDetails deliveryDetailsId) {
        this.deliveryDetailsId = deliveryDetailsId;
    }

    public Shipment getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(Shipment shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Payment getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Payment paymentId) {
        this.paymentId = paymentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
