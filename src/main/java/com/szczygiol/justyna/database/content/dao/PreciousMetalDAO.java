package com.szczygiol.justyna.database.content.dao;

import com.szczygiol.justyna.database.content.objects.PreciousMetal;
import com.szczygiol.justyna.database.content.util.DAOException;
import com.szczygiol.justyna.database.content.util.DataAccessObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PreciousMetalDAO extends DataAccessObject<PreciousMetal> {
    private static final String GET_ONE = "SELECT id, type, description, carats, purity, price_variable FROM precious_metal WHERE id = ?";
    private static final String FIND_ALL = "SELECT id, type, description, carats, purity, price_variable FROM precious_metal";
    private static final String INSERT = "INSERT INTO precious_metal (type, description, carats, purity, price_variable) VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE precious_metal SET type = ?, description = ?, carats = ?, purity = ?, price_variable = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM precious_metal WHERE id = ?";


    public PreciousMetalDAO(Connection connection) {
        super(connection);
    }

    @Override
    public PreciousMetal findById(long id) throws DAOException, SQLException {
        PreciousMetal pm = new PreciousMetal();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE);) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    pm.setId(rs.getLong("id"));
                    pm.setType(rs.getString("type"));
                    pm.setDescription(rs.getString("description"));
                    pm.setCarats(rs.getInt("carats"));
                    pm.setPurity(rs.getInt("purity"));
                    pm.setPriceVariable(rs.getDouble("price_variable"));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("No object of id " + id + " have been found in the precious_metal", e);
        } finally {
            connection.close();
        }
        return pm;
    }

    @Override
    public List<PreciousMetal> findAll() throws DAOException, SQLException {
        List<PreciousMetal> metals = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    PreciousMetal pm = new PreciousMetal();
                    pm.setId(rs.getLong("id"));
                    pm.setType(rs.getString("type"));
                    pm.setDescription(rs.getString("description"));
                    pm.setCarats(rs.getInt("carats"));
                    pm.setPurity(rs.getInt("purity"));
                    pm.setPriceVariable(rs.getDouble("price_variable"));
                    metals.add(pm);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Read data from precious_metal failed", e);
        } finally {
            connection.close();
        }
        return metals;
    }

    @Override
    public PreciousMetal update(PreciousMetal dto) throws DAOException, SQLException {
        PreciousMetal pm;
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE)) {
            statement.setString(1, dto.getType());
            statement.setString(2, dto.getDescription());
            statement.setInt(3, dto.getCarats());
            statement.setInt(4, dto.getPurity());
            statement.setDouble(5, dto.getPriceVariable());
            statement.setLong(6, dto.getId());
            statement.execute();
            pm = this.findById(dto.getId());
        } catch (SQLException e) {
            throw new DAOException("Updating precious_metal failed", e);
        } finally {
            connection.close();
        }
        return pm;
    }

    @Override
    public PreciousMetal create(PreciousMetal dto) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, dto.getType());
            statement.setString(2, dto.getDescription());
            statement.setInt(3, dto.getCarats());
            statement.setInt(4, dto.getPurity());
            statement.setDouble(5, dto.getPriceVariable());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    dto.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Creating precious_metal failed", e);
        } finally {
            connection.close();
        }
            return dto;
    }

    @Override
    public void delete(long id) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Deleting precious_metal failed", e);
        } finally {
            connection.close();
        }
    }
}
