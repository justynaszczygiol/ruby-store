package com.szczygiol.justyna.database.content.objects;

import com.szczygiol.justyna.database.content.util.DataTransferObject;

public class OrdersJewelry implements DataTransferObject {
    private long id;
    private Orders orders_id;
    private Jewelry jewelry_id;

    public OrdersJewelry() {
    }

    public OrdersJewelry(Orders orders_id, Jewelry jewelry_id) {
        this.orders_id = orders_id;
        this.jewelry_id = jewelry_id;
    }

    @Override
    public String toString() {
        return "id: " + id + " |" +
                " orders_id: " + orders_id.getStatus() + " |" +
                " jewelry_id: " + jewelry_id.getDescription() + '\n';
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Orders getOrders_id() {
        return orders_id;
    }

    public void setOrders_id(Orders orders_id) {
        this.orders_id = orders_id;
    }

    public Jewelry getJewelry_id() {
        return jewelry_id;
    }

    public void setJewelry_id(Jewelry jewelry_id) {
        this.jewelry_id = jewelry_id;
    }
}
