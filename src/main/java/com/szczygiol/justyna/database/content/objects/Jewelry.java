package com.szczygiol.justyna.database.content.objects;

import com.szczygiol.justyna.database.content.util.DataTransferObject;

public class Jewelry implements DataTransferObject {
    private long id;
    private String description;
    private Style styleId;
    private Gem mainGem;
    private Gem secondaryGem;
    private PreciousMetal preciousMetal;
    private String engraverText;
    private double price;

    @Override
    public String toString() {
        return  "id: " + id + " |" +
                " description: " + description + " |" +
                " style_id: " + styleId +  " |" +
                " main_gem: " + mainGem +  " |" +
                " secondary_gem: " + secondaryGem + " |" +
                " precious_metal: " + preciousMetal +   " |" +
                " engraver_text: " + engraverText +  " |" +
                " price: " + price + '\n';
    }

    public Jewelry() {
    }

    public Jewelry(Style styleId, Gem mainGem, Gem secondaryGem, PreciousMetal preciousMetal, String engraverText) {
        this.styleId = styleId;
        this.mainGem = mainGem;
        this.secondaryGem = secondaryGem;
        this.preciousMetal = preciousMetal;
        this.engraverText = engraverText;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        if (mainGem != null && secondaryGem != null) {
            if (styleId.getCollection() != null) {
                description = preciousMetal.getDescription() + " " + styleId.getType() + " with " +
                        styleId.getMainGemQuantity() + " " + mainGem.getDescription() +
                        " and " + styleId.getSecondaryGemQuantity() + " " + secondaryGem.getDescription() +
                        " from " + styleId.getCollection() + " collection.";
            } else {
                description = preciousMetal.getDescription() + " " + styleId.getType() + " with " +
                        styleId.getMainGemQuantity() + " " + mainGem.getDescription() +
                        " and " + styleId.getSecondaryGemQuantity() + " " + secondaryGem.getDescription() + ".";
            }
        } else if (mainGem != null && secondaryGem == null) {
            if (styleId.getCollection() != null) {
                description = preciousMetal.getDescription() + " " + styleId.getType() + " with " +
                        styleId.getMainGemQuantity() + " " + mainGem.getDescription() +
                        " from " + styleId.getCollection() + " collection.";
            } else {
                description = preciousMetal.getDescription() + " " + styleId.getType() + " with " +
                        styleId.getMainGemQuantity() + " " + mainGem.getDescription() + ".";
            }
        }
        return description.toLowerCase();
    }

    public Style getStyleId() {
        return styleId;
    }

    public void setStyleId(Style styleId) {
        this.styleId = styleId;
    }

    public Gem getMainGem() {
        return mainGem;
    }

    public void setMainGem(Gem mainGem) {
            this.mainGem = mainGem;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Gem getSecondaryGem() {
        return secondaryGem;
    }

    public void setSecondaryGem(Gem secondaryGem) {
        this.secondaryGem = secondaryGem;
    }

    public PreciousMetal getPreciousMetal() {
        return preciousMetal;
    }

    public void setPreciousMetal(PreciousMetal preciousMetal) {
        this.preciousMetal = preciousMetal;
    }

    public String getEngraverText() {
        return engraverText;
    }

    public void setEngraverText(String engraverText) {
        if (styleId.isEngraver()) {
            this.engraverText = engraverText;
        } else {
            this.engraverText = null;
        }
    }

    public double getPrice() {
        if (mainGem != null && secondaryGem != null) {
            price = styleId.getPreciousMetalVariable() * preciousMetal.getPriceVariable() +
                    styleId.getMainGemVariable() * mainGem.getPriceVariable() +
                    styleId.getSecondaryGemVariable() * secondaryGem.getPriceVariable();
        } else if (mainGem != null && secondaryGem == null) {
            price = styleId.getPreciousMetalVariable() * preciousMetal.getPriceVariable() +
                    styleId.getMainGemVariable() * mainGem.getPriceVariable();
        } else if (mainGem == null && secondaryGem == null) {
            price = styleId.getPreciousMetalVariable() * preciousMetal.getPriceVariable();
        }
        return price;
    }
}
