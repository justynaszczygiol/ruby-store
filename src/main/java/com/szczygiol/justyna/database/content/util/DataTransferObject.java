package com.szczygiol.justyna.database.content.util;

public interface DataTransferObject<O> {
    long getId();
}
