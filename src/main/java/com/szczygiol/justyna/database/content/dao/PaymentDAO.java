package com.szczygiol.justyna.database.content.dao;

import com.szczygiol.justyna.database.content.objects.Payment;
import com.szczygiol.justyna.database.content.util.DAOException;
import com.szczygiol.justyna.database.content.util.DataAccessObject;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class PaymentDAO extends DataAccessObject<Payment> {
    private static final String INSERT = "INSERT INTO payment (date, method, status) VALUES (?, ?, ?)";
    private static final String GET_ONE = "SELECT id, date, method, status FROM payment WHERE id = ?";
    private static final String FIND_ALL = "SELECT id, date, method, status FROM payment";
    private static final String UPDATE = "UPDATE payment SET status = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM payment WHERE id = ?";

    public PaymentDAO(Connection connection) {
        super(connection);
    }


    @Override
    public Payment findById(long id) throws DAOException, SQLException {
        Payment payment = new Payment();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE)) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    payment.setId(rs.getLong("id"));
                    payment.setDate(rs.getTimestamp("date"));
                    payment.setMethod(rs.getString("method"));
                    payment.setStatus(rs.getString("status"));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("No object of id " + id + " have been found in the payment", e);
        } finally {
            connection.close();
        }
        return payment;
    }

    @Override
    public List<Payment> findAll() throws DAOException, SQLException {
        List<Payment> payments = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Payment payment = new Payment();
                    payment.setId(rs.getLong("id"));
                    payment.setDate(rs.getTimestamp("date"));
                    payment.setMethod(rs.getString("method"));
                    payment.setStatus(rs.getString("status"));
                    payments.add(payment);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Read data from payment failed", e);
        } finally {
            connection.close();
        }
        return payments;
    }

    @Override
    public Payment update(Payment dto) throws DAOException, SQLException {
        Payment payment;
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE)) {
            statement.setString(1, dto.getStatus());
            statement.setLong(2, dto.getId());
            statement.execute();
            payment = this.findById(dto.getId());
        } catch (SQLException e) {
            throw new DAOException("Updating payment failed", e);
        } finally {
            connection.close();
        }
        return payment;
    }

    @Override
    public Payment create(Payment dto) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setTimestamp(1, Timestamp.from(Instant.now()));
            statement.setString(2, dto.getMethod());
            statement.setString(3, dto.getStatus());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    dto.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Creating payment failed", e);
        } finally {
            connection.close();
        }
        return dto;
    }

        @Override
    public void delete(long id) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Deleting payment failed", e);
        } finally {
            connection.close();
        }
    }
}
