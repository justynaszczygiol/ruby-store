package com.szczygiol.justyna.database.content.dao;

import com.szczygiol.justyna.database.content.objects.*;
import com.szczygiol.justyna.database.content.util.DAOException;
import com.szczygiol.justyna.database.content.util.DataAccessObject;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JewelryDAO extends DataAccessObject<Jewelry> {
    private static final String INSERT = "INSERT INTO jewelry(description, style_id, main_gem, " +
            "secondary_gem, precious_metal, engraver_text, price) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String GET_ONE = "SELECT id, description, style_id, main_gem, secondary_gem, " +
            "precious_metal, engraver_text, price FROM jewelry WHERE id = ?";
    private static final String UPDATE = "UPDATE jewelry SET description = ?, style_id = ?, main_gem = ?, " +
            "secondary_gem = ?, precious_metal = ?, engraver_text = ?, price = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM jewelry WHERE id = ?";
    private static final String FIND_ALL = "SELECT id, description, style_id, main_gem, secondary_gem, " +
            "precious_metal, engraver_text, price FROM jewelry";

    public JewelryDAO(Connection connection) {
        super(connection);
    }

    @Override
    public Jewelry findById(long id) throws DAOException, SQLException {
        Jewelry jewelry = new Jewelry();
        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE)) {
            statement.setLong(1, id);
          try (ResultSet rs = statement.executeQuery()) {
              while (rs.next()) {
                  jewelry.setId(rs.getLong("id"));
                  jewelry.setDescription(rs.getString("description"));
                  StyleDAO styleDAO = new StyleDAO(connection);
                  jewelry.setStyleId(styleDAO.findById(rs.getLong("style_id")));
                  GemDAO gemDAO = new GemDAO(connection);
                  jewelry.setMainGem(gemDAO.findById(rs.getLong("main_gem")));
                  jewelry.setSecondaryGem(gemDAO.findById(rs.getLong("secondary_gem")));
                  PreciousMetalDAO metalDAO = new PreciousMetalDAO(connection);
                  jewelry.setPreciousMetal(metalDAO.findById(rs.getLong("precious_metal")));
                  jewelry.setEngraverText(rs.getString("engraver_text"));
                  jewelry.setPrice(rs.getDouble("price"));
              }
          }
        } catch (SQLException e) {
            throw new DAOException("No object of id " + id + " have been found in the jewelry", e);
        } finally {
            connection.close();
        }
        return jewelry;
    }

    @Override
    public List<Jewelry> findAll() throws DAOException, SQLException {
        List<Jewelry> jewelryList = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ALL)) {
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Jewelry jewelry = new Jewelry();
                    jewelry.setId(rs.getLong("id"));
                    jewelry.setDescription(rs.getString("description"));
                    StyleDAO styleDAO = new StyleDAO(connection);
                    jewelry.setStyleId(styleDAO.findById(rs.getLong("style_id")));
                    GemDAO gemDAO = new GemDAO(connection);
                    jewelry.setMainGem(gemDAO.findById(rs.getLong("main_gem")));
                    jewelry.setSecondaryGem(gemDAO.findById(rs.getLong("secondary_gem")));
                    PreciousMetalDAO metalDAO = new PreciousMetalDAO(connection);
                    jewelry.setPreciousMetal(metalDAO.findById(rs.getLong("precious_metal")));
                    jewelry.setEngraverText(rs.getString("engraver_text"));
                    jewelry.setPrice(rs.getDouble("price"));
                    jewelryList.add(jewelry);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Read data from jewelry failed", e);
        } finally {
            connection.close();
        }
        return jewelryList;
    }

    @Override
    public Jewelry update(Jewelry dto) throws DAOException, SQLException {
        Jewelry jewelry;
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE)) {
            statement.setString(1, dto.getDescription());
            statement.setLong(2, dto.getStyleId().getId());
            statement.setLong(3, dto.getMainGem().getId());
            statement.setLong(4, dto.getSecondaryGem().getId());
            statement.setLong(5, dto.getPreciousMetal().getId());
            statement.setString(6, dto.getEngraverText());
            statement.setDouble(7, dto.getPrice());
            statement.setLong(8, dto.getId());
            statement.execute();
            jewelry = this.findById(dto.getId());
        } catch (SQLException e) {
            throw new DAOException("Updating jewelry failed", e);
        } finally {
            connection.close();
        }
        return jewelry;
    }

    @Override
    public Jewelry create(Jewelry dto) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, dto.getDescription());
            statement.setLong(2, dto.getStyleId().getId());
            if (dto.getStyleId().getMainGemQuantity() != 0) {
                statement.setLong(3, dto.getMainGem().getId());
            } else {
                statement.setNull(3, Types.BIGINT);
            }
            if (dto.getStyleId().getSecondaryGemQuantity() != 0) {
                statement.setLong(4, dto.getSecondaryGem().getId());
            } else {
                statement.setNull(4, Types.BIGINT);
            }
            statement.setLong(5, dto.getPreciousMetal().getId());
            statement.setString(6, dto.getEngraverText());
            statement.setDouble(7, dto.getPrice());
            statement.execute();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    dto.setId(keys.getLong(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Creating jewelry failed", e);
        } finally {
            connection.close();
        }
        return dto;
    }

        @Override
    public void delete(long id) throws DAOException, SQLException {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Deleting jewelry failed", e);
        } finally {
            connection.close();
        }
    }
}
